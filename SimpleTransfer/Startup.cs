using Google.Cloud.Diagnostics.AspNetCore;
using Google.Cloud.PubSub.V1;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SimpleTransfer.Data;
using SimpleTransfer.Data.Repositories;
using StackExchange.Redis;

namespace SimpleTransfer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddGoogleExceptionLogging(options =>
            {
                options.ProjectId = "cloudunitproject";
                options.ServiceName = "SimpleTransfer";
                options.Version = "1.0.0";
            });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddDefaultIdentity<IdentityUser>()
                .AddEntityFrameworkStores<ApplicationDbContext>();
            services.AddRouting(options => options.LowercaseUrls = true);
            services.AddControllersWithViews();
            services.AddRazorPages();

            services.AddAuthentication()
                .AddGoogle(google =>
                {
                    google.ClientId = Configuration["GoogleOAuth:ClientId"];
                    google.ClientSecret = Configuration["GoogleOAuth:ClientSecret"];
                });

            // https://stackexchange.github.io/StackExchange.Redis/Basics
            var redisConnection = Configuration.GetConnectionString("RedisConnection");
            var redis = ConnectionMultiplexer.Connect(redisConnection);
            services.AddSingleton<IConnectionMultiplexer>(redis);

            services.AddScoped<CloudStorageRepository>();
            services.AddScoped<RedisRepository>();
            services.AddScoped<PubSubRepository>();

            var host = Configuration["SMTP:host"];
            var username = Configuration["SMTP:username"];
            var password = Configuration["SMTP:password"];
            var emailSender = new EmailSender(host, username, password);
            services.AddSingleton(emailSender);

            var crypto = new Cryptography(
                projectId: "cloudunitproject",
                locationId: "global",
                keyRingId: "simple-transfer-key-ring",
                cryptoKeyId: "simple-transfer");
            services.AddSingleton(crypto);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            // Use before handling any requests to ensure all unhandled exceptions are reported.
            app.UseGoogleExceptionLogging();

            loggerFactory.AddGoogle(app.ApplicationServices, "cloudunitproject");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });

            // setup pub-sub
            PublisherServiceApiClient publisher = PublisherServiceApiClient.Create();
            try
            {
                var _ = publisher.GetTopic(PubSubRepository.TopicName);
            }
            catch
            {
                var _ = publisher.CreateTopic(PubSubRepository.TopicName);
            }

            SubscriberServiceApiClient subscriber = SubscriberServiceApiClient.Create();
            try
            {
                var _ = subscriber.GetSubscription(PubSubRepository.SubscriptionName);
            }
            catch
            {
                var _ = subscriber.CreateSubscription(PubSubRepository.SubscriptionName, PubSubRepository.TopicName, null, 30);
            }
        }
    }
}
