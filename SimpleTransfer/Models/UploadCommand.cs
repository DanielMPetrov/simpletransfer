﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace SimpleTransfer.Models
{
    public class UploadCommand
    {
        [Required]
        public string FileName { get; set; }

        [Required]
        public IFormFile FormFile { get; set; }
    }
}
