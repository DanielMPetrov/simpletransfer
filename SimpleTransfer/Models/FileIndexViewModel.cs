﻿using System;
using System.Collections.Generic;

namespace SimpleTransfer.Models
{
    public class FileIndexViewModel
    {
        public IEnumerable<File> Files { get; set; }

        public class File
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public DateTime LastModified { get; set; }

            public string Type { get; set; }
        }
    }
}
