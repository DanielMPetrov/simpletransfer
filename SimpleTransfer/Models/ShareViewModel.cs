﻿using System.ComponentModel.DataAnnotations;

namespace SimpleTransfer.Models
{
    public class ShareViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        [Required, EmailAddress]
        public string Email { get; set; }

        public string Message { get; set; }
    }
}
