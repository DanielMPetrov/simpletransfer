﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SimpleTransfer.Data.Repositories;
using System.Threading.Tasks;

namespace SimpleTransfer.Controllers
{
    public class CronController : Controller
    {
        private readonly PubSubRepository pubSub;
        private readonly ILogger<CronController> logger;

        public CronController(PubSubRepository pubSub, ILogger<CronController> logger)
        {
            this.pubSub = pubSub;
            this.logger = logger;
        }

        [Route("[controller]/send-emails")]
        public async Task SendEmails()
        {
            await pubSub.DequeueAndSendEmailsAsync();

            logger.LogInformation("Send emails cron job executed successfully.");
        }
    }
}
