﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SimpleTransfer.Data;
using SimpleTransfer.Data.Entities;
using SimpleTransfer.Data.Repositories;
using SimpleTransfer.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleTransfer.Controllers
{
    [Authorize]
    public class FilesController : Controller
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly ApplicationDbContext context;
        private readonly CloudStorageRepository cloudStorage;
        private readonly RedisRepository redis;
        private readonly PubSubRepository pubSub;

        public FilesController(
            UserManager<IdentityUser> userManager,
            ApplicationDbContext context,
            CloudStorageRepository cloudStorage,
            RedisRepository redis,
            PubSubRepository pubSub)
        {
            this.userManager = userManager;
            this.context = context;
            this.cloudStorage = cloudStorage;
            this.redis = redis;
            this.pubSub = pubSub;
        }

        public IActionResult Index()
        {
            var files = redis.GetFiles(User.Identity.Name);

            var model = new FileIndexViewModel { Files = files };

            return View(model);
        }

        public IActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Upload(UploadCommand command)
        {
            if (!ModelState.IsValid)
            {
                return View(command);
            }

            var user = await userManager.FindByNameAsync(User.Identity.Name);
            var blob = await cloudStorage.UploadAsync(command.FormFile, user.Email);

            var file = new File
            {
                Name = command.FileName,
                Type = command.FormFile.ContentType,
                LastModified = blob.Updated ?? DateTime.Now,
                StorageLocation = $"https://storage.cloud.google.com/simple-transfer/{blob.Name}",
                Owner = user
            };

            context.Files.Add(file);

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                await cloudStorage.DeleteAsync(blob);
                throw;
            }

            await redis.UpdateCache(User.Identity.Name);

            return RedirectToAction("Index", "Home");
        }

        [Route("files/{id:min(0)}/share")]
        public IActionResult Share(int id)
        {
            var name = context.Files.Where(file => file.Id == id).Select(file => file.Name).FirstOrDefault();

            if (name is null)
            {
                return NotFound();
            }

            var model = new ShareViewModel
            {
                Name = name
            };

            return View(model);
        }

        [HttpPost("files/{id:min(0)}/share")]
        public async Task<IActionResult> Share(ShareViewModel model)
        {
            var file = context.Files.Find(model.Id);

            if (file is null)
            {
                return NotFound();
            }

            if (!ModelState.IsValid)
            {
                model.Name = file.Name;
                return View(model);
            }

            var success = await cloudStorage.AddReaderAsync(file.StorageLocation.Split('/').Last(), model.Email);

            if (!success)
            {
                ModelState.AddModelError(nameof(model.Email), "You can only share files with valid Gmail users.");
                model.Name = file.Name;
                return View(model);
            }

            await pubSub.EnqueueEmailAsync(model.Email, file.StorageLocation, model.Message);

            TempData["Success"] = $"File '{file.Name}' sent to <a class=\"alert-link\" href=\"mailto:{model.Email}\">{model.Email}</a> successfully.";
            return RedirectToAction(nameof(Index));
        }

        [AllowAnonymous]
        [Route("files/{id:min(0)}/download")]
        public IActionResult Download(int id, [FromServices] ILogger<FilesController> logger)
        {
            var file = context.Files.Find(id);

            if (file is null)
            {
                return NotFound();
            }

            if (User.Identity.IsAuthenticated)
            {
                logger.LogInformation($"User '{User.Identity.Name}' downloaded file #{id}");
            }
            else
            {
                logger.LogInformation($"Anonymous user downloaded file #{id}");
            }

            return Redirect(file.StorageLocation);
        }
    }
}
