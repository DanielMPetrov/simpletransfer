﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using SimpleTransfer.Data.Repositories;
using SimpleTransfer.Models;
using System.Diagnostics;

namespace SimpleTransfer.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Files");
            }

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            // log uncaught exceptions when not in development mode
            var feature = HttpContext.Features.Get<IExceptionHandlerFeature>();

            if (feature is object)
            {
                ExceptionLogger.Log(feature.Error);
            }

            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
