﻿using Microsoft.AspNetCore.Identity;
using System;

namespace SimpleTransfer.Data.Entities
{
    public class File
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public DateTime LastModified { get; set; }

        public string StorageLocation { get; set; }

        public virtual IdentityUser Owner { get; set; }
    }
}
