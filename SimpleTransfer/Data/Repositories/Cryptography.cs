﻿using Google.Cloud.Kms.V1;
using Google.Protobuf;
using System;

namespace SimpleTransfer.Data.Repositories
{
    public class Cryptography
    {
        private readonly string projectId;
        private readonly string locationId;
        private readonly string keyRingId;
        private readonly string cryptoKeyId;

        public Cryptography(string projectId, string locationId, string keyRingId, string cryptoKeyId)
        {
            this.projectId = projectId;
            this.locationId = locationId;
            this.keyRingId = keyRingId;
            this.cryptoKeyId = cryptoKeyId;
        }

        public string Encrypt(string plain)
        {
            var client = KeyManagementServiceClient.Create();
            var cryptoKeyName = new CryptoKeyName(projectId, locationId, keyRingId, cryptoKeyId);

            var pathName = CryptoKeyPathName.Parse(cryptoKeyName.ToString());
            var response = client.Encrypt(pathName, ByteString.CopyFromUtf8(plain));

            var result = response.Ciphertext.ToBase64();

            return result;
        }

        public string Decrypt(string cipher)
        {
            var client = KeyManagementServiceClient.Create();
            var cryptoKeyName = new CryptoKeyName(projectId, locationId, keyRingId, cryptoKeyId);

            var bytes = Convert.FromBase64String(cipher);
            var response = client.Decrypt(cryptoKeyName, ByteString.CopyFrom(bytes));

            var result = response.Plaintext.ToStringUtf8();

            return result;
        }
    }
}
