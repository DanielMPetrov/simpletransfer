﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SimpleTransfer.Models;
using StackExchange.Redis;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace SimpleTransfer.Data.Repositories
{
    public class RedisRepository
    {
        private readonly IDatabase redisDatabase;
        private readonly UserManager<IdentityUser> userManager;
        private readonly ApplicationDbContext context;

        public RedisRepository(
            IConnectionMultiplexer multiplexer,
            UserManager<IdentityUser> userManager,
            ApplicationDbContext context)
        {
            redisDatabase = multiplexer.GetDatabase();
            this.userManager = userManager;
            this.context = context;
        }

        public async Task UpdateCache(string username)
        {
            var user = await userManager.FindByNameAsync(username);

            var files = await context.Files
                .Where(file => file.Owner == user)
                .OrderByDescending(file => file.LastModified)
                .ToListAsync();

            var fileViewModels = files.Select(f => new FileIndexViewModel.File
            {
                Id = f.Id,
                Name = f.Name,
                LastModified = f.LastModified,
                Type = f.Type,
            });

            // update redis cache
            string serialised = JsonSerializer.Serialize(fileViewModels);
            redisDatabase.StringSet(username, serialised);
        }

        public IEnumerable<FileIndexViewModel.File> GetFiles(string username)
        {
            if (!redisDatabase.KeyExists(username))
            {
                return Enumerable.Empty<FileIndexViewModel.File>();
            }

            var serialised = redisDatabase.StringGet(username);
            var fileViewModels = JsonSerializer.Deserialize<IEnumerable<FileIndexViewModel.File>>(serialised);
            return fileViewModels;
        }
    }
}
