﻿using MailKit.Net.Smtp;
using MimeKit;
using System.Threading.Tasks;

namespace SimpleTransfer.Data.Repositories
{
    public class EmailSender
    {
        private readonly string host;
        private readonly int port;
        private readonly string username;
        private readonly string password;

        public EmailSender(string host, string username, string password)
            : this(host, 587, username, password)
        {
        }

        public EmailSender(string host, int port, string username, string password)
        {
            this.host = host;
            this.port = port;
            this.username = username;
            this.password = password;
        }

        public async Task SendEmailAsync(string email, string message)
        {
            var mime = new MimeMessage();
            mime.From.Add(new MailboxAddress("Simple Transfer - No Reply", "noreply@simpletransfer.com"));
            mime.To.Add(new MailboxAddress(email));
            mime.Subject = "Someone shared a file with you!";
            mime.Body = new TextPart("html") { Text = message };

            using var smtp = new SmtpClient();

            await smtp.ConnectAsync(host, port, false);

            await smtp.AuthenticateAsync(username, password);

            await smtp.SendAsync(mime);
            await smtp.DisconnectAsync(true);
        }
    }
}
