﻿using Google.Api.Gax.ResourceNames;
using Google.Cloud.ErrorReporting.V1Beta1;
using System;

namespace SimpleTransfer.Data.Repositories
{
    public static class ExceptionLogger
    {
        public static void Log(Exception e)
        {
            var client = ReportErrorsServiceClient.Create();
            var projectName = new ProjectName("cloudunitproject");
            var error = new ReportedErrorEvent
            {
                Message = e.Message,

                ServiceContext = new ServiceContext
                {
                    Service = "SimpleTransfer",
                    Version = "1.0.0"
                }
            };
            client.ReportErrorEvent(projectName, error);
        }
    }
}
