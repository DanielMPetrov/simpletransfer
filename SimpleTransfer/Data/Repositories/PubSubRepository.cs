﻿using Google.Cloud.PubSub.V1;
using Google.Protobuf;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleTransfer.Data.Repositories
{
    public class PubSubRepository
    {
        private const string ProjectName = "CloudUnitProject";

        public static readonly TopicName TopicName =
            new TopicName(ProjectName, "file-share-email-topic");

        public static readonly SubscriptionName SubscriptionName =
            new SubscriptionName(ProjectName, "file-share-email-subscription");

        private readonly EmailSender emailSender;
        private readonly Cryptography cryptography;

        public PubSubRepository(EmailSender emailSender, Cryptography cryptography)
        {
            this.emailSender = emailSender;
            this.cryptography = cryptography;
        }

        public async Task EnqueueEmailAsync(string email, string fileUrl, string message)
        {
            var client = await PublisherServiceApiClient.CreateAsync();
            var topic = await client.GetTopicAsync(TopicName);
            var pubSubMessage = new PubsubMessage();
            if (!string.IsNullOrWhiteSpace(message))
            {
                pubSubMessage.Data = ByteString.CopyFromUtf8(cryptography.Encrypt(message));
            }
            pubSubMessage.Attributes.Add("email", cryptography.Encrypt(email));
            pubSubMessage.Attributes.Add("fileUrl", cryptography.Encrypt(fileUrl));

            var _ = await client.PublishAsync(topic.TopicName, new[] { pubSubMessage });
        }

        public async Task DequeueAndSendEmailsAsync()
        {
            var client = await SubscriberServiceApiClient.CreateAsync();
            var pullResponse = await client.PullAsync(SubscriptionName, returnImmediately: true, maxMessages: 10);

            if (!pullResponse.ReceivedMessages.Any())
            {
                return;
            }

            var ackIds = new List<string>(pullResponse.ReceivedMessages.Count);

            foreach (var receivedMessage in pullResponse.ReceivedMessages)
            {
                await SendEmailAsync(receivedMessage);
                ackIds.Add(receivedMessage.AckId);
            }

            await client.AcknowledgeAsync(SubscriptionName, ackIds);
        }

        private async Task SendEmailAsync(ReceivedMessage receivedMessage)
        {
            string email = cryptography.Decrypt(receivedMessage.Message.Attributes["email"]);
            string fileUrl = cryptography.Decrypt(receivedMessage.Message.Attributes["fileUrl"]);

            var body = new StringBuilder();
            body.Append("<p>Hi there!</p>");
            body.Append("<p>Someone shared a file with you!</p>");

            if (!receivedMessage.Message.Data.IsEmpty)
            {
                body.Append("<p>They left the following message:</p>");
                string message = cryptography.Decrypt(receivedMessage.Message.Data.ToStringUtf8());
                body.Append($"<p><em>{message}</em></p>");
            }

            body.Append($"<p>To directly access the file, <a href=\"{fileUrl}\">CLICK HERE</a></p>");
            body.Append("<p>-- The SimpleTransfer team</p>");

            await emailSender.SendEmailAsync(email, body.ToString());
        }
    }
}
