﻿using Google;
using Google.Apis.Storage.v1.Data;
using Google.Cloud.Storage.V1;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleTransfer.Data.Repositories
{
    public class CloudStorageRepository
    {
        private const string BucketName = "simple-transfer";

        public async Task<Google.Apis.Storage.v1.Data.Object> UploadAsync(IFormFile formFile, string owner)
        {
            using var memory = new MemoryStream();
            await formFile.CopyToAsync(memory);

            using var storage = StorageClient.Create();
            var name = Guid.NewGuid().ToString() + Path.GetExtension(formFile.FileName);
            var blob = await storage.UploadObjectAsync(BucketName, name, formFile.ContentType, memory,
                options: new UploadObjectOptions { Projection = Projection.Full });

            blob.Acl ??= new List<ObjectAccessControl>();
            blob.Acl.Add(new ObjectAccessControl
            {
                Bucket = BucketName,
                Entity = $"user-{owner}",
                Role = "OWNER",
            });

            blob = await storage.UpdateObjectAsync(blob, new UpdateObjectOptions
            {
                // Avoid race conditions.
                IfMetagenerationMatch = blob.Metageneration,
            });

            return blob;
        }

        public async Task DeleteAsync(Google.Apis.Storage.v1.Data.Object @object)
        {
            using var storage = StorageClient.Create();
            await storage.DeleteObjectAsync(@object);
        }

        public async Task<bool> AddReaderAsync(string name, string email)
        {
            using var storage = await StorageClient.CreateAsync();

            var blob = await storage.GetObjectAsync(BucketName, name,
                new GetObjectOptions { Projection = Projection.Full });

            if (blob.Acl.Any(x => x.Email == email))
            {
                return true;
            }

            blob.Acl.Add(new ObjectAccessControl
            {
                Bucket = BucketName,
                Entity = $"user-{email}",
                Role = "READER",
            });

            try
            {
                await storage.UpdateObjectAsync(blob, new UpdateObjectOptions
                {
                    // Avoid race conditions.
                    IfMetagenerationMatch = blob.Metageneration,
                });
            }
            catch (GoogleApiException)
            {
                return false;
            }

            return true;
        }
    }
}
