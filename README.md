# SimpleTransfer
A simple file transfer website.

### Development setup

1. In Visual Studio 2019, `right click` on the project file -> `Manage User Secrets` and add the following secrets.

```json
{
  "ConnectionStrings": {
    "DefaultConnection": "...",
    "RedisConnection": "..."
  },
  "GoogleOAuth": {
    "ClientId": "...",
    "ClientSecret": "..."
  },
  "SMTP": {
    "host": "...",
    "username": "...",
    "password": "..."
  }
}
```

2. In `launchSettings.json` replace `GOOGLE_APPLICATION_CREDENTIALS` with the local file path to the Google exported json credentials.

3. Run the application.
